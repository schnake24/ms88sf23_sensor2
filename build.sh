#!/bin/bash

./make_date.sh
west build -p always -b ms88sf2_nrf52840 -- -DCMAKE_BUILD_TYPE=Debug -Dmcuboot_CONFIG_BOOT_SIGNATURE_KEY_FILE=\\\"$(pwd)/root-ec-p256.pem\\\" -Dmcuboot_CONFIG_SERIAL=y -Dmcuboot_CONFIG_UART_CONSOLE=y
