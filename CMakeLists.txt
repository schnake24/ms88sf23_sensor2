#
# Copyright (c) 2020 Nordic Semiconductor ASA
#
# SPDX-License-Identifier: LicenseRef-BSD-5-Clause-Nordic
#

cmake_minimum_required(VERSION 3.20.0)

set(OVERLAY_CONFIG version.conf)

#set(default_build_type "Release")
set(NO_BUILD_TYPE_WARNING 1)

if(CMAKE_BUILD_TYPE STREQUAL Release)
  set(CONF_FILE "prj_prod.conf")
  message("Build release")
endif(CMAKE_BUILD_TYPE STREQUAL Release)

#list(APPEND BOARD_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/boards)
set(BOARD_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/.)
find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})

project("Zigbee light bulb")

# NORDIC SDK APP START
target_sources(app PRIVATE
  src/main.c
  src/battery.c
  src/zb_zcl_pressure_measurement.c
)

target_include_directories(app PRIVATE
  include
)

# NORDIC SDK APP END
