#!/bin/bash

echo Generate version infos

d=$(date +"%Y%m%d")
h=$(git log --pretty=format:'%h' -n 1)

v=$(git describe --tags --abbrev=0)

cat > include/version.h <<+END
#ifndef _VERSION_H
#define _VERSION_H

#define BUILD_DATE "$d"
#define COMMIT_HASH "$h"
#define APP_VERSION "$v"

#endif
+END

cat > version.conf <<+END
CONFIG_MCUBOOT_IMAGE_VERSION="$v"
+END
