#!/bin/bash
NAME=`basename -s .yaml $1`
echo "Will flash config $NAME"

nrfutil zigbee production_config --offset 0xf9000 ${NAME}.yaml ${NAME}.hex
nrfjprog --program ${NAME}.hex --sectorerase --verify
