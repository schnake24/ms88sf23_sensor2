#include <stdbool.h>
#include <stdint.h>

#include <zephyr/types.h>
#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/sensor.h>
#include <drivers/gpio.h>
#include <drivers/watchdog.h>
#include <ram_pwrdn.h>
#include <logging/log.h>
#include <hal/nrf_power.h>
#include <soc.h>
#include "nrf_802154.h"

#include <nrfx_saadc.h>

#include <zboss_api.h>
#include <zboss_api_addons.h>
#include <zigbee/zigbee_app_utils.h>
#include <zigbee/zigbee_error_handler.h>
#include <zb_nrf_platform.h>
#include "zb_mem_config_custom.h"

#include <zigbee/zigbee_fota.h>
#include <sys/reboot.h>
#include <dfu/mcuboot.h>



#include "battery.h"


/**
 * Define if watch should be used
 */
#define xUSE_APP_WATCHDOG

/**
 * Min and max of mV for battery
 */
#define MAX_BAT 4200
#define MIN_BAT 3100

/* LED indicating OTA Client Activity. */
//#define OTA_ACTIVITY_LED          DK_LED2

#if DT_HAS_COMPAT_STATUS_OKAY(sensirion_sht4x)
#define BUILD_SHT4X
#include <drivers/sensor/sht4x.h>
#include "zb_multi_sensor_sht4x.h"
#else
#warning "No senstirion, sht4x compatible node found n device tree, disable build"
#endif

#if DT_HAS_COMPAT_STATUS_OKAY(bosch_bme280)
#define BUILD_BME280
#include "zb_multi_sensor_bme280.h"
#else
#warning "No bosch, bme280 compatible node found n device tree, disable build"
#endif

#define MAX_ERRORS 3

#define REFV 600  // 600mV
#define RES_BIT 12 // 12 Bit resolution
#define GAIN 1/2 // 1/2 Gain

#define SAADC_CHANNEL_COUNT   1

#define STACKSIZE 1024
#define PRIORITY 7

//#define ZB_SCHED_SLEEP_THRESHOLD_MS 10

#ifdef IEEE_ADDR
//const static zb_ieee_addr_t g_ed_addr = {0x01, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x02};
const static zb_ieee_addr_t g_ed_addr = IEEE_ADDR;
#endif

static int m_isConnected;

K_EVENT_DEFINE(m_bootEvent);
#define EVENT_SENSOR_OKAY 0b0001
#define EVENT_BATTERY_OKAY 0b0010
#define EVENT_ZIGBEE_OKAY 0b0100

LOG_MODULE_REGISTER(app);

static sensor_device_ctx_t m_dev_ctx;
static volatile int m_total_errors;

#ifdef USE_APP_WATCHDOG
static int m_wdtChannel;
const static struct device *wdt;
static struct wdt_timeout_cfg m_wdt_config;
#endif

#define WDT_DEV_NAME DT_LABEL(BME280)

ZB_ZCL_DECLARE_IDENTIFY_ATTRIB_LIST(
	identify_attr_list,
	&m_dev_ctx.identify_attr.identify_time);

ZB_ZCL_DECLARE_BASIC_ATTRIB_LIST_EXT(
	basic_attr_list,
	&m_dev_ctx.basic_attr.zcl_version,
	&m_dev_ctx.basic_attr.app_version,
	&m_dev_ctx.basic_attr.stack_version,
	&m_dev_ctx.basic_attr.hw_version,
	m_dev_ctx.basic_attr.mf_name,
	m_dev_ctx.basic_attr.model_id,
	m_dev_ctx.basic_attr.date_code,
	&m_dev_ctx.basic_attr.power_source,
	m_dev_ctx.basic_attr.location_id,
	&m_dev_ctx.basic_attr.ph_env,
	m_dev_ctx.basic_attr.sw_ver);


ZB_ZCL_DECLARE_TEMP_MEASUREMENT_ATTRIB_LIST(
	temperature_attr_list, 
	&m_dev_ctx.temp_attr.measure_value,
	&m_dev_ctx.temp_attr.min_measure_value, 
	&m_dev_ctx.temp_attr.max_measure_value, 
	&m_dev_ctx.temp_attr.tolerance);

ZB_ZCL_DECLARE_REL_HUMIDITY_MEASUREMENT_ATTRIB_LIST(
	rel_humidity_measurement_attr_list, 
	&m_dev_ctx.rel_hum_attr.measure_value, 
	&m_dev_ctx.rel_hum_attr.min_measure_value, 
	&m_dev_ctx.rel_hum_attr.max_measure_value);

#ifdef BUILD_BME280
ZB_ZCL_DECLARE_PRESSURE_MEASUREMENT_ATTRIB_LIST(
	pressure_attr_list, 
	&m_dev_ctx.pres_attr.measure_value,
	&m_dev_ctx.pres_attr.min_measure_value, 
	&m_dev_ctx.pres_attr.max_measure_value, 
	&m_dev_ctx.pres_attr.tolerance);
#endif

#define bat_num 
ZB_ZCL_DECLARE_POWER_CONFIG_BATTERY_ATTRIB_LIST_EXT(
	power_config_attr_list,
	&m_dev_ctx.power_attr.battery_voltage,
	&m_dev_ctx.power_attr.battery_size,
	&m_dev_ctx.power_attr.battery_quantity,
	&m_dev_ctx.power_attr.battery_rated_voltage,
	&m_dev_ctx.power_attr.battery_alarm_mask,
	&m_dev_ctx.power_attr.battery_voltage_min_threshold,
	&m_dev_ctx.power_attr.battery_percentage_remaining,
	&m_dev_ctx.power_attr.battery_voltage_threshold1,
	&m_dev_ctx.power_attr.battery_voltage_threshold2,
	&m_dev_ctx.power_attr.battery_voltage_threshold3,
	&m_dev_ctx.power_attr.battery_percentage_min_threshold,
	&m_dev_ctx.power_attr.battery_percentage_threshold1,
	&m_dev_ctx.power_attr.battery_percentage_threshold2,
	&m_dev_ctx.power_attr.battery_percentage_threshold3,
	&m_dev_ctx.power_attr.battery_alarm_state);

#ifdef BUILD_BME280
ZB_DECLARE_MULTI_SENSOR_CLUSTER_LIST(
	multi_sensor_clusters,
	basic_attr_list,
	identify_attr_list,
	temperature_attr_list,
	rel_humidity_measurement_attr_list,
	pressure_attr_list,
	power_config_attr_list);
#else
ZB_DECLARE_MULTI_SENSOR_CLUSTER_LIST(
	multi_sensor_clusters,
	basic_attr_list,
	identify_attr_list,
	temperature_attr_list,
	rel_humidity_measurement_attr_list,
	power_config_attr_list);
#endif

ZB_ZCL_DECLARE_MULTI_SENSOR_EP(
	multi_sensor_ep,
	MULTI_SENSOR_ENDPOINT,
	multi_sensor_clusters);

extern zb_af_endpoint_desc_t zigbee_fota_client_ep;
ZBOSS_DECLARE_DEVICE_CTX_2_EP(
	multi_sensor_ctx,
	zigbee_fota_client_ep,
	multi_sensor_ep);

/*
extern zb_af_endpoint_desc_t ota_upgrade_client_ep;
ZBOSS_DECLARE_DEVICE_CTX_2_EP(
	multi_sensor_ctx,
	ota_upgrade_client_ep,
	multi_sensor_ep);
*/


static void sensor_clusters_attr_init(void)
{
	m_dev_ctx.basic_attr.zcl_version   = ZB_ZCL_VERSION;
	m_dev_ctx.basic_attr.app_version   = SENSOR_INIT_BASIC_APP_VERSION;
	m_dev_ctx.basic_attr.stack_version = SENSOR_INIT_BASIC_STACK_VERSION;
	m_dev_ctx.basic_attr.hw_version    = SENSOR_INIT_BASIC_HW_VERSION;

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.mf_name,
		SENSOR_INIT_BASIC_MANUF_NAME,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_MANUF_NAME));

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.model_id,
		SENSOR_INIT_BASIC_MODEL_ID,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_MODEL_ID));

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.date_code,
		SENSOR_INIT_BASIC_DATE_CODE,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_DATE_CODE));

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.sw_ver,
		COMMIT_HASH,
		ZB_ZCL_STRING_CONST_SIZE(COMMIT_HASH));

	m_dev_ctx.basic_attr.power_source = SENSOR_INIT_BASIC_POWER_SOURCE;

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.location_id,
		SENSOR_INIT_BASIC_LOCATION_DESC,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_LOCATION_DESC));

	m_dev_ctx.basic_attr.ph_env = SENSOR_INIT_BASIC_PH_ENV;

	m_dev_ctx.identify_attr.identify_time = ZB_ZCL_IDENTIFY_IDENTIFY_TIME_DEFAULT_VALUE;

	m_dev_ctx.temp_attr.measure_value = (zb_int16_t)0;
	m_dev_ctx.temp_attr.min_measure_value = (zb_int16_t)-4000;
	m_dev_ctx.temp_attr.max_measure_value = (zb_int16_t)10000;
	m_dev_ctx.temp_attr.tolerance = (zb_int16_t)0;

	m_dev_ctx.rel_hum_attr.measure_value = 0;
	m_dev_ctx.rel_hum_attr.min_measure_value = 0;
	m_dev_ctx.rel_hum_attr.max_measure_value = 10000;

#ifdef BUILD_BME280
	m_dev_ctx.pres_attr.measure_value = (zb_int16_t)0;
	m_dev_ctx.pres_attr.min_measure_value = (zb_int16_t)2000;
	m_dev_ctx.pres_attr.max_measure_value = (zb_int16_t)15000;
	m_dev_ctx.pres_attr.tolerance = (zb_int16_t)0;
#endif

	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.rel_hum_attr.measure_value,
		ZB_FALSE);

#ifdef BUILD_BME280
	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_PRESSURE_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_PRESSURE_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.pres_attr.measure_value,
		ZB_FALSE);
#endif

	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.temp_attr.measure_value,
		ZB_FALSE);
}



/*
 * Should init the reporting cluster but stil not working
 */
static void configure_attribute_reporting()
{
	LOG_INF("Config reporting");

	zb_zcl_reporting_info_t temp_rep_info;
	memset(&temp_rep_info, 0, sizeof(temp_rep_info));

	temp_rep_info.direction = ZB_ZCL_CONFIGURE_REPORTING_SEND_REPORT;
	temp_rep_info.ep = MULTI_SENSOR_ENDPOINT;
	temp_rep_info.cluster_id = ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT;
	temp_rep_info.cluster_role = ZB_ZCL_CLUSTER_SERVER_ROLE;
	temp_rep_info.attr_id = ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_ID;
	temp_rep_info.dst.profile_id = ZB_AF_HA_PROFILE_ID;
	temp_rep_info.u.send_info.min_interval = 30;      // 30 seconds
	temp_rep_info.u.send_info.max_interval = 300;    // 5 minutes
	temp_rep_info.u.send_info.delta.s16 = 0x0001;        // 0.5 degrees



	zb_zcl_reporting_info_t humid_rep_info;
	memset(&humid_rep_info, 0, sizeof(humid_rep_info));

	humid_rep_info.direction = ZB_ZCL_CONFIGURE_REPORTING_SEND_REPORT;
	humid_rep_info.ep = MULTI_SENSOR_ENDPOINT;
	humid_rep_info.cluster_id = ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT;
	humid_rep_info.cluster_role = ZB_ZCL_CLUSTER_SERVER_ROLE;
	humid_rep_info.attr_id = ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_ID;
	humid_rep_info.dst.profile_id = ZB_AF_HA_PROFILE_ID;
	humid_rep_info.u.send_info.min_interval = 30;       // 60 seconds
	humid_rep_info.u.send_info.max_interval = 300;     // 10 minutes
	humid_rep_info.u.send_info.delta.u16 = 0x0001;         // 1% rh



#ifdef BUILD_BME280
	zb_zcl_reporting_info_t press_rep_info;
	memset(&press_rep_info, 0, sizeof(press_rep_info));

	press_rep_info.direction = ZB_ZCL_CONFIGURE_REPORTING_SEND_REPORT;
	press_rep_info.ep = MULTI_SENSOR_ENDPOINT;
	press_rep_info.cluster_id = ZB_ZCL_CLUSTER_ID_PRESSURE_MEASUREMENT;
	press_rep_info.cluster_role = ZB_ZCL_CLUSTER_SERVER_ROLE;
	press_rep_info.attr_id = ZB_ZCL_ATTR_PRESSURE_MEASUREMENT_VALUE_ID;
	press_rep_info.dst.profile_id = ZB_AF_HA_PROFILE_ID;
	press_rep_info.u.send_info.min_interval = 30;       // 30 seconds
	press_rep_info.u.send_info.max_interval = 900;     // 15 minutes
	press_rep_info.u.send_info.delta.u16 = 0x0001;         // 1% rh
#endif


	// Prior to SDK 3.1 this function was zb_zcl_put_default_reporting_info(zb_zcl_reporting_info_t*)
	zb_zcl_put_reporting_info(&temp_rep_info, ZB_TRUE);    // Assume the override parameter here is to override the new default configuration.
	zb_zcl_put_reporting_info(&humid_rep_info, ZB_TRUE);
#ifdef BUILD_BME280
	zb_zcl_put_reporting_info(&press_rep_info, ZB_TRUE);
#endif
}



#ifdef USE_APP_WATCHDOG
void initWatchdog()
{
	LOG_INF("Starting watchdog");

	wdt = device_get_binding(WDT_DEV_NAME);
	if(!wdt) {
		LOG_ERR("Could not get watchdog device");
		return;
	}

	m_wdt_config.callback = NULL;
	m_wdt_config.flags = WDT_FLAG_RESET_SOC;
	m_wdt_config.window.min = 0;
	m_wdt_config.window.max = 120 * 1000;

	m_wdtChannel = wdt_install_timeout(wdt, &m_wdt_config);
	if(m_wdtChannel == -ENOTSUP) {
		LOG_ERR("Watchdog timeout config not suppoted");
	}
	if(m_wdtChannel < 0) {
		LOG_ERR("Watchdog install error");
		return;
	}
}

void startWatchdog()
{
	int error_code = wdt_setup(wdt, 0);
	if(error_code < 0) {
		LOG_ERR("Watchdog setup error");
		return;
	}
}

void feedWatchdog()
{
	int error = wdt_feed(wdt, m_wdtChannel);
	if(error) {
		LOG_ERR("Error by feeding watchdog");
		return;
	}
}
#endif


void setZigbeeBatteryPercent(int32_t percent)
{
		m_dev_ctx.power_attr.battery_percentage_remaining = (100 - percent) * 2;
		ZB_ZCL_SET_ATTRIBUTE(
			MULTI_SENSOR_ENDPOINT,
			ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
			ZB_ZCL_CLUSTER_SERVER_ROLE,
			ZB_ZCL_ATTR_POWER_CONFIG_BATTERY_PERCENTAGE_REMAINING_ID,
			(zb_uint8_t *)&m_dev_ctx.power_attr.battery_percentage_remaining,
			ZB_TRUE);
}

void setZigbeeBatteryVoltage(int32_t voltage)
{
		m_dev_ctx.power_attr.battery_voltage = voltage / 100;
		ZB_ZCL_SET_ATTRIBUTE(
			MULTI_SENSOR_ENDPOINT,
			ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
			ZB_ZCL_CLUSTER_SERVER_ROLE,
			ZB_ZCL_ATTR_POWER_CONFIG_BATTERY_VOLTAGE_ID,
			(zb_uint8_t *)&m_dev_ctx.power_attr.battery_voltage,
			ZB_TRUE);
}

#ifdef BUILD_BME280
void setZigbeePressure(float pressure)
{
	m_dev_ctx.pres_attr.measure_value = (zb_int16_t)(pressure * 10.0);
	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_PRESSURE_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_PRESSURE_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.pres_attr.measure_value,
		ZB_FALSE);
}
#endif

void setZigbeeTemp(float temp)
{
	m_dev_ctx.temp_attr.measure_value = (zb_int16_t)(temp * 100.0);
	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.temp_attr.measure_value,
		ZB_FALSE);
}

void setZigbeeHuminity(float hum)
{
	m_dev_ctx.rel_hum_attr.measure_value = (zb_uint16_t)(hum * 100.0);
	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.rel_hum_attr.measure_value,
		ZB_FALSE);
}

static void battery_workHandler(struct k_work *work)
{
	ARG_UNUSED(work);
	int32_t adc, v;

	k_msleep(2000); // Just wait 2 seconds before go
	LOG_INF("Battery work started");
	battery_init();
	k_msleep(200);

	LOG_INF("Read battery ADC value");
	adc = battery_readAdc();
	v = battery_calcV(adc);

	battery_uninit();

	int32_t range = MAX_BAT - MIN_BAT;
	int32_t rel = MAX_BAT - v;
	int32_t now = 100 * rel / range;

	if(now > 100) now = 100;
	if(now < 0) now = 0;

	LOG_INF("Battery: %d (%dmV - %d)", adc, v, 100 - now);

	setZigbeeBatteryPercent(now);
	setZigbeeBatteryVoltage(v);

	k_event_post(&m_bootEvent, EVENT_BATTERY_OKAY);
}
K_WORK_DEFINE(battery_work, battery_workHandler);

static void battery_timerHandler(struct k_timer *timer)
{
	ARG_UNUSED(timer);
	k_work_submit(&battery_work);
}
K_TIMER_DEFINE(battery_timer, battery_timerHandler, NULL);



#ifdef BUILD_BME280
static void bme280_workHandler(struct k_work *work)
{
	ARG_UNUSED(work);
	LOG_INF("BME280 work initialized");

	const struct device *dev = DEVICE_DT_GET_ANY(bosch_bme280);
	if(device_is_ready(dev)) {
		LOG_INF("Found device bosch bme280\n");
		struct sensor_value sensor_temp, sensor_press, sensor_humidity;
		float temp, press, hum;

		sensor_sample_fetch(dev);
		sensor_channel_get(dev, SENSOR_CHAN_AMBIENT_TEMP, &sensor_temp);
		sensor_channel_get(dev, SENSOR_CHAN_PRESS, &sensor_press);
		sensor_channel_get(dev, SENSOR_CHAN_HUMIDITY, &sensor_humidity);

		temp = sensor_temp.val1 + (sensor_temp.val2 / 1000000.0);
		press = (sensor_press.val1 + (sensor_press.val2 / 1000000.0)) * 10.0;
		hum = sensor_humidity.val1 + (sensor_humidity.val2 / 1000000.0);
		LOG_INF("Zigbee - temp: %f; press: %f; hum: %f", temp, press, hum);

		setZigbeeTemp(temp);
		setZigbeeHuminity(hum);
		setZigbeePressure(press);
		LOG_INF("Zigbee - send as temp: %d; press: %d; hum: %d", m_dev_ctx.temp_attr.measure_value, m_dev_ctx.pres_attr.measure_value, m_dev_ctx.rel_hum_attr.measure_value);

		k_event_post(&m_bootEvent, EVENT_SENSOR_OKAY);
	} else {
		LOG_ERR("No device bosch bme280 found; did initialization fail?\n");
	}

}
K_WORK_DEFINE(bme280_work, bme280_workHandler);

static void bme280_timerHandler(struct k_timer *timer)
{
	ARG_UNUSED(timer);
	k_work_submit(&bme280_work);
}
K_TIMER_DEFINE(bme280_timer, bme280_timerHandler, NULL);
#endif


#ifdef BUILD_SHT4X
static void sht4x_workHandler(struct k_work *work)
{
	ARG_UNUSED(work);
	LOG_INF("SHT4X work intialized");

	const struct device *dev = DEVICE_DT_GET_ANY(sensirion_sht4x);
	if(device_is_ready(dev)) {
		LOG_INF("Found device sensirion sht4x\n");
		
		struct sensor_value sensor_temp, sensor_hum;
		sensor_sample_fetch(dev);

		sensor_channel_get(dev, SENSOR_CHAN_AMBIENT_TEMP, &sensor_temp);
		sensor_channel_get(dev, SENSOR_CHAN_HUMIDITY, &sensor_hum);
		LOG_INF("Read from SHT4X Temp: %d.%d °C and %d.%d RH\n", sensor_temp.val1, sensor_temp.val2, sensor_hum.val1, sensor_hum.val2);

		float temp = sensor_temp.val1 + (sensor_temp.val2 / 1000000.0);
		float hum = sensor_hum.val1 + (sensor_hum.val2 / 1000000.0);
		LOG_INF("Zigbee - temp: %f; hum: %f", temp, hum);

		setZigbeeTemp(temp);
		setZigbeeHuminity(hum);

		k_event_post(&m_bootEvent, EVENT_SENSOR_OKAY);
	} else {
		LOG_ERR("No device sensirion sht4x found; did initialization failed?\n");
	}
}
K_WORK_DEFINE(sht4x_work, sht4x_workHandler);

static void sht4x_timerHandler(struct k_timer *timer) {
	ARG_UNUSED(timer);
	LOG_INF("SHT4X timer handler");
	k_work_submit(&sht4x_work);
}
K_TIMER_DEFINE(sht4x_timer, sht4x_timerHandler, NULL);
#endif



void zboss_signal_handler(zb_bufid_t bufid)
{
	zb_zdo_app_signal_hdr_t *sig_hndler = NULL;
	zb_zdo_app_signal_type_t sig = zb_get_app_signal(bufid, &sig_hndler);
	zb_ret_t status = ZB_GET_APP_SIGNAL_STATUS(bufid);

	if(status != 0) {
		if(++m_total_errors > MAX_ERRORS) {
			LOG_WRN("Reboot device, too many errors");
			NVIC_SystemReset();
		}
	}
	// LOG_DBG("Status: %d, (%d)", status, sig);

#ifdef CONFIG_ZIGBEE_FOTA
	/* Pass signal to the OTA client implementation. */
	zigbee_fota_signal_handler(bufid);
#endif

#ifdef USE_APP_WATCHDOG
	if(m_isConnected) {
		feedWatchdog();
	}
#endif

	switch(sig) {
		case ZB_BDB_SIGNAL_DEVICE_FIRST_START:
		case ZB_BDB_SIGNAL_DEVICE_REBOOT:
		case ZB_BDB_SIGNAL_STEERING:
			// configure_attribute_reporting();
			LOG_INF("Joined network, reported from callback");
			if(status == RET_OK) {
				k_event_post(&m_bootEvent, EVENT_ZIGBEE_OKAY);
				m_isConnected = 1;
			} else {
				m_isConnected = 0;
			}
			break;
		case ZB_ZDO_SIGNAL_LEAVE:
			m_isConnected = 0;
			break;
		case ZB_BDB_SIGNAL_WWAH_REJOIN_STARTED:
			if(++m_total_errors > MAX_ERRORS) {
				LOG_WRN("Reboot device, too many errors");
				NVIC_SystemReset();
			}
			break;
		case ZB_ZDO_SIGNAL_LEAVE_INDICATION:
			if(++m_total_errors > MAX_ERRORS) {
				LOG_WRN("Reboot device, too many errors");
				NVIC_SystemReset();
			}
			break;
		case ZB_NLME_STATUS_INDICATION:
			if(status!=RET_OK) {
				LOG_WRN("Failed status, reseting");
				NVIC_SystemReset();
			}
		default:
			break;
	}
	ZB_ERROR_CHECK(zigbee_default_signal_handler(bufid));

	if(bufid) {
		zb_buf_free(bufid);
	}
}


/*
static void NotReportedCb(zb_uint8_t ep, zb_uint16_t cluster_id, zb_uint16_t attr_id)
{
	LOG_INF("Attribute not reported %d, %d, %d", ep, cluster_id, attr_id);
}
*/


static void confirm_image(void)
{
	if (!boot_is_img_confirmed()) {
		int ret = boot_write_img_confirmed();

		if (ret) {
			LOG_ERR("Couldn't confirm image: %d", ret);
		} else {
			LOG_INF("Marked image as OK");
		}
	} else {
		LOG_INF("Bootimage is confirmed");
	}
}

static void ota_evt_handler(const struct zigbee_fota_evt *evt)
{
	LOG_DBG("FOTA event handler evt_id: %d", evt->id);
	switch (evt->id) {
		case ZIGBEE_FOTA_EVT_PROGRESS:
			//dk_set_led(OTA_ACTIVITY_LED, evt->dl.progress % 2);
			LOG_INF("Progress %d", evt->dl.progress);
			break;

		case ZIGBEE_FOTA_EVT_FINISHED:
			LOG_INF("Reboot application.");
			if(IS_ENABLED(CONFIG_RAM_POWER_DOWN_LIBRARY)) {
				power_up_unused_ram();
			}
			sys_reboot(SYS_REBOOT_COLD);
			break;

		case ZIGBEE_FOTA_EVT_ERROR:
			LOG_ERR("OTA image transfer failed.");
			break;

		default:
			break;
	}
}



void initLeds()
{
}



void main(void)
{
	LOG_INF("Starting schnake app for sensor2");

	nrf_power_dcdcen_set(NRF_POWER, 0);
	nrf_power_dcdcen_vddh_set(NRF_POWER, 0);

	m_total_errors = 0;
	m_isConnected = 0;

	zigbee_erase_persistent_storage(false);

#ifdef USE_APP_WATCHDOG
	initWatchdog();
#endif

/*
	zb_set_ed_timeout(ED_AGING_TIMEOUT_64MIN);
	zb_set_keepalive_timeout(ZB_MILLISECONDS_TO_BEACON_INTERVAL(60000));
	zb_zdo_pim_set_long_poll_interval(ZB_MILLISECONDS_TO_BEACON_INTERVAL(60000));
*/
	zb_set_rx_on_when_idle(ZB_FALSE);
#ifdef IEEE_ADDR
	zb_set_long_address(g_ed_addr);
#endif
	//zb_sleep_set_threshold(ZB_SCHED_SLEEP_THRESHOLD_MS);

	power_down_unused_ram();
	zigbee_configure_sleepy_behavior(true);

	/* Initialize Zigbee FOTA download service. */
	zigbee_fota_init(ota_evt_handler);

	/* Mark the current firmware as valid. */
	//confirm_image();

	/* Register callback for handling ZCL commands. */
	ZB_ZCL_REGISTER_DEVICE_CB(zigbee_fota_zcl_cb);

	ZB_AF_REGISTER_DEVICE_CTX(&multi_sensor_ctx);
	sensor_clusters_attr_init();
	initLeds();

#ifdef USE_APP_WATCHDOG
	startWatchdog();
#endif

	zigbee_enable();

	k_timer_start(&battery_timer, K_SECONDS(90), K_MINUTES(15));

#ifdef BUILD_BME280
	k_timer_start(&bme280_timer, K_SECONDS(15), K_SECONDS(60));
#endif

#ifdef BUILD_SHT4X
	k_timer_start(&sht4x_timer, K_SECONDS(15), K_SECONDS(60));
#endif

	LOG_INF("Wait for subsystems to say okay");
	k_event_wait_all(&m_bootEvent, EVENT_BATTERY_OKAY | EVENT_SENSOR_OKAY | EVENT_ZIGBEE_OKAY, true, K_FOREVER);

	LOG_INF("All subsystems seems stable, commit boot image");
	/* Mark the current firmware as valid. */
	confirm_image();
}

static int board_ms88sfe2_nrf52840_init(const struct device *dev)
{
	ARG_UNUSED(dev);

	/* if the nrf52840dongle_nrf52840 board is powered from USB
		* (high voltage mode), GPIO output voltage is set to 1.8 volts by
		* default and that is not enough to turn the green and blue LEDs on.
		* Increase GPIO voltage to 3.0 volts.
		*/
	if((nrf_power_mainregstatus_get(NRF_POWER) == NRF_POWER_MAINREGSTATUS_HIGH) && ((NRF_UICR->REGOUT0 & UICR_REGOUT0_VOUT_Msk) != (UICR_REGOUT0_VOUT_1V8 << UICR_REGOUT0_VOUT_Pos))) {
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
		while(NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}

		NRF_UICR->REGOUT0 = (NRF_UICR->REGOUT0 & ~((uint32_t)UICR_REGOUT0_VOUT_Msk)) | (UICR_REGOUT0_VOUT_1V8 << UICR_REGOUT0_VOUT_Pos);

		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
		while(NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}

		/* a reset is required for changes to take effect */
		NVIC_SystemReset();
	}

	return 0;
}

SYS_INIT(board_ms88sfe2_nrf52840_init, PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEFAULT);
