#include <stdint.h>

#include <zephyr/types.h>
#include <zephyr.h>
#include <device.h>
#include <drivers/sensor.h>
#include <logging/log.h>
#include <nrfx_saadc.h>

#include "battery.h"

#define SAADC_CHANNEL_COUNT   1

#define REFV 600  // 600mV
#define RES_BIT 12 // 12 Bit resolution
#define GAIN 1/2 // 1/2 Gain

#define APP_ERROR_CHECK(e) if(e != NRFX_SUCCESS) LOG_ERR("Fault, unhandled error")

static nrf_saadc_value_t samples[SAADC_CHANNEL_COUNT];
static nrfx_saadc_channel_t channels[SAADC_CHANNEL_COUNT] = {NRFX_SAADC_DEFAULT_CHANNEL_SE(NRF_SAADC_INPUT_VDDHDIV5, 0)};

LOG_MODULE_REGISTER(battery);

void battery_init(void)
{
	nrfx_err_t err_code;

	LOG_INF("Battery init");

    err_code = nrfx_saadc_init(6);
	APP_ERROR_CHECK(err_code);

	channels[0].channel_config.gain = NRF_SAADC_GAIN1_2;
	err_code = nrfx_saadc_channels_config(channels, SAADC_CHANNEL_COUNT);
	APP_ERROR_CHECK(err_code);

	err_code = nrfx_saadc_offset_calibrate(NULL);
}

void battery_uninit(void)
{
		nrfx_saadc_uninit();
}

int32_t battery_readAdc()
{
    nrfx_err_t err_code;

    err_code = nrfx_saadc_simple_mode_set((1<<0), NRF_SAADC_RESOLUTION_12BIT, NRF_SAADC_OVERSAMPLE_16X, NULL);
    APP_ERROR_CHECK(err_code);

    err_code = nrfx_saadc_buffer_set(samples, SAADC_CHANNEL_COUNT);
    APP_ERROR_CHECK(err_code);

    err_code = nrfx_saadc_mode_trigger();
    APP_ERROR_CHECK(err_code);

    return samples[0];
}

int32_t battery_calcV(int32_t adc)
{
	int32_t v;
	v = ((adc * REFV) / ((1<<RES_BIT) * GAIN)) * 5;
	return v;
}
